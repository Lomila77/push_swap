/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/10 12:10:40 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/10 12:10:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "libft/libft.h"

void	init_top_and_bot(t_ring *a, t_ring *b, int len)
{
	if (len != 1)
	{
		a->top = len / 2 - 1;
		a->bot = len / 2;
	}
	else
	{
		a->top = 0;
		a->bot = 1;
	}
	if (b != NULL)
	{
		b->top = 0;
		b->bot = b->top;
	}
}

void	stack_cursor(int len, int *param, int mode, t_ring *stack)
{
	if (mode == TO_BOT)
	{
		if (stack != NULL && *param == stack->bot)
			*param = stack->top;
		else if (*param == 0)
			*param = len;
		else
			*param -= 1;
	}
	else if (mode == TO_TOP)
	{
		if (stack != NULL && *param == stack->top)
			*param = stack->bot;
		else if (*param == len)
			*param = 0;
		else
			*param += 1;
	}
}

void	ring_cursor(int len, int *param, int mode, t_ring *stack)
{
	stack_cursor(len, param, mode, stack);
	if (*param == stack->bot)
		stack_cursor(len, param, mode, stack);
}

int	empty_stack(t_ring *stack)
{
	if (stack->top == stack->bot)
		return (YES);
	return (NO);
}

int	get_biggest_val(t_ring *stack, int len)
{
	int	i;
	int	j;

	i = stack->top;
	j = i;
	stack->biggest = stack->nb[i];
	while (i != stack->bot)
	{
		if (stack->nb[i] > stack->biggest)
		{
			stack->biggest = stack->nb[i];
			j = i;
		}
		stack_cursor(len, &i, TO_BOT, stack);
	}
	return (j);
}
