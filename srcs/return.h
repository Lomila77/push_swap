/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   return.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 11:36:36 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/15 11:36:36 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RETURN_H
# define RETURN_H

# include "data.h"

int	error(int fd, t_stack *stack);
int	success(t_stack *stack);
int	all_free(t_stack *stack);

#endif
