/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/05 18:52:32 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/05 18:52:32 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATA_H
# define DATA_H

/* In parsing.c */

# define CHECK 0
# define NO_CHECK 1

# define SORT -2

/* In first_stage_algo.c */

# define VALUE_START 0
# define START_SORT 1
# define LENTH 2
# define DIRECTION 3

/* In second_stage_algo.c */

# define R_R 0
# define RR_RR 1
# define RRA_RB 2
# define RA_RRB 3

/* In utils_bis.c */

# define R 0
# define RR 1

/* In utils_algo.c */

# define BOT 0
# define TOP 1

/* In utils_algo_ter.c */

# define MORE 0
# define LESS 1

# define NB 3

/* In utils.c */

# define TO_TOP 1
# define TO_BOT -1

typedef struct s_move
{
	int	instruction_a;
	int	instruction_b;
	int	total;
	int	a;
	int	a_r;
	int	a_rr;
	int	b;
	int	b_r;
	int	b_rr;
	int	cursor;
	int	value;
}	t_move;

typedef struct s_indice
{
	int	start;
	int	after;
}	t_indice;

typedef struct s_ring
{
	int	*nb;
	int	top;
	int	bot;
	int	smallest;
	int	biggest;
	int	start;
}	t_ring;

typedef struct s_stack
{
	t_ring	a;
	t_ring	b;
}	t_stack;

typedef struct s_checker
{
	char			here;
	char			*instruction;
}	t_checker;

#endif
