/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instruction.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 15:46:17 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/08 15:46:17 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "instruction.h"
#include "libft/libft.h"

int	call_instruction(t_stack *stack, char *instruction, int len)
{
	if (ft_strncmp(instruction, "sa", 2) == 0)
		return (s(&stack->a, len, NULL));
	if (ft_strncmp(instruction, "sb", 2) == 0)
		return (s(&stack->b, len, NULL));
	if (ft_strncmp(instruction, "ss", 2) == 0)
		return (ss(stack, len, NULL));
	if (ft_strncmp(instruction, "rra", 3) == 0)
		return (rr_(&stack->a, len, NULL));
	if (ft_strncmp(instruction, "rrb", 3) == 0)
		return (rr_(&stack->b, len, NULL));
	if (ft_strncmp(instruction, "rrr", 3) == 0)
		return (rrr(stack, len, NULL));
	if (ft_strncmp(instruction, "ra", 2) == 0)
		return (r(&stack->a, len, NULL));
	if (ft_strncmp(instruction, "rb", 2) == 0)
		return (r(&stack->b, len, NULL));
	if (ft_strncmp(instruction, "rr", 2) == 0)
		return (rr(stack, len, NULL));
	if (ft_strncmp(instruction, "pb", 2) == 0)
		return (p(&stack->a, &stack->b, len, NULL));
	if (ft_strncmp(instruction, "pa", 2) == 0)
		return (p(&stack->b, &stack->a, len, NULL));
	return (ERROR);
}
