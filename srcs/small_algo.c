/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   small_algo.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/15 16:39:51 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/15 16:39:51 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "algo.h"
#include "utils.h"
#include "instruction.h"
#include "libft/libft.h"

void	init_var_sort3(t_indice *cursor, t_stack *stack, int len)
{
	cursor->start = stack->a.top;
	stack_cursor(len, &cursor->start, TO_BOT, NULL);
	cursor->after = cursor->start;
	stack_cursor(len, &cursor->after, TO_BOT, NULL);
	stack->a.smallest = get_smallest_val(&stack->a, len);
}

void	sort_3(t_stack *stack, int len)
{
	t_indice	cursor;

	init_var_sort3(&cursor, stack, len);
	if (stack->a.nb[stack->a.top] > stack->a.nb[cursor.start]
		&& stack->a.nb[stack->a.top] > stack->a.nb[cursor.after]
		&& stack->a.nb[cursor.start] == stack->a.nb[stack->a.smallest])
		r(&stack->a, len, "a");
	if (stack->a.nb[cursor.start] > stack->a.nb[stack->a.top]
		&& stack->a.nb[cursor.start] > stack->a.nb[cursor.after]
		&& stack->a.nb[stack->a.top] < stack->a.nb[cursor.after])
	{
		r(&stack->a, len, "a");
		s(&stack->a, len, "a");
		rr_(&stack->a, len, "a");
	}
	if (stack->a.nb[stack->a.top] > stack->a.nb[cursor.start])
		s(&stack->a, len, "a");
	stack->a.smallest = get_smallest_val(&stack->a, len);
	if (stack->a.nb[stack->a.top] != stack->a.nb[stack->a.smallest])
	{
		if (stack->a.nb[stack->a.smallest] == stack->a.nb[cursor.start])
			r(&stack->a, len, "a");
		else
			rr_(&stack->a, len, "a");
	}
}
