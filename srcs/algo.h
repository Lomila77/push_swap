/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/21 12:00:11 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/21 12:00:11 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ALGO_H
# define ALGO_H

# include "data.h"

/* First stage Algo */
int		sort_a(t_stack *stack, int len);
void	copy_list_sort(t_ring *stack, int *list_sort, int is_sort[3], int len);
void	clear_a(t_stack *stack, int len, int *list_sort, int is_sort[3]);

/* Second stage Algo */
t_move	is_to_insert(t_stack *stack, int len);
void	nb_move_for_insert(t_stack *stack, int len, t_move *ret);
void	update_stack_for_insert(t_stack *stack, int len, t_move *mv,
			char *name);
int		insert(t_stack *stack, int len, t_move *move);

/* Small Algo */
void	sort_3(t_stack *stack, int len);

#endif
