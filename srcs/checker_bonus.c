/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_bonus.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/06 16:08:53 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/16 12:22:12 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker_bonus.h"
#include "utils.h"
#include "libft/libft.h"
#include "return.h"
#include "instruction.h"

#include <unistd.h>

int	check_sort(t_stack *stack, int len)
{
	int	cursor;
	int	tmp;

	cursor = stack->a.top;
	tmp = stack->a.nb[cursor];
	stack_cursor(len, &cursor, TO_BOT, NULL);
	while (cursor != stack->a.bot)
	{
		if (tmp > stack->a.nb[cursor])
		{
			free(stack->a.nb);
			free(stack->b.nb);
			return (error(STDIN, NULL));
		}
		tmp = stack->a.nb[cursor];
		stack_cursor(len, &cursor, TO_BOT, NULL);
	}
	if (empty_stack(&stack->b) == NO)
		return (error(STDIN, stack));
	return (success(stack));
}

int	mult_free_err(char *f1, char *f2, char *f3, char *f4)
{
	if (f1 != NULL)
		ft_free(&f1);
	if (f2 != NULL)
		ft_free(&f2);
	if (f3 != NULL)
		ft_free(&f3);
	if (f4 != NULL)
		ft_free(&f4);
	return (ERROR);
}

int	got_instruction(t_stack *stack, int len, char *tmp[2], char *buffer)
{
	tmp[1] = ft_strlnew(tmp[1], '\n');
	if (call_instruction(stack, tmp[1], len) == ERROR)
		return (mult_free_err(buffer, tmp[1], NULL, NULL));
	ft_free(&tmp[1]);
	return (SUCCESS);
}

int	get_instruction(t_stack *stack, int len)
{
	int		read_ret;
	char	*buffer;
	char	*tmp[2];

	be_null2(&tmp[0], &tmp[1]);
	read_ret = 1;
	buffer = ft_calloc(2, sizeof(char));
	while (n_read(STDIN, &buffer, 1, &read_ret) > 0)
	{
		tmp[0] = ft_strjoin(tmp[1], buffer);
		ft_free(&tmp[1]);
		if (tmp[0] == NULL)
			return (ERROR);
		tmp[1] = tmp[0];
		if (str_have_c(tmp[1], '\n') == YES)
		{
			if (got_instruction(stack, len, tmp, buffer) != SUCCESS)
				return (ERROR);
		}
	}
	ft_free(&buffer);
	if (read_ret == ERROR)
		return (ERROR);
	return (SUCCESS);
}
