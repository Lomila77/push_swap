/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   first_stage_algo.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/21 11:56:49 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/16 12:15:55 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "algo.h"
#include "utils.h"
#include "instruction.h"
#include "libft/libft.h"
#include "return.h"

/* Sort stack A and push others in B */
int	sort_a(t_stack *stack, int len)
{
	int	is_sort[4];
	int	*list_sort;

	count_list_sort(is_sort, &stack->a, len);
	count_list_sort_rev(is_sort, &stack->a, len);
	list_sort = malloc(sizeof(int) * is_sort[LENTH]);
	copy_list_sort(&stack->a, list_sort, is_sort, len);
	clear_a(stack, len, list_sort, is_sort);
	free(list_sort);
	return (is_sort[VALUE_START]);
}

void	more(int is_sort[4], t_ring *stack, int *list_sort, int len)
{
	t_indice	cursor;
	int			i;

	i = 1;
	cursor.start = is_sort[START_SORT];
	cursor.after = cursor.start;
	ring_cursor(len, &cursor.after, TO_BOT, stack);
	while (cursor.after != is_sort[START_SORT])
	{
		if (stack->nb[cursor.start] < stack->nb[cursor.after])
		{
			list_sort[i] = stack->nb[cursor.after];
			i++;
			cursor.start = cursor.after;
		}
		ring_cursor(len, &cursor.after, TO_BOT, stack);
	}
}

void	less(int is_sort[4], t_ring *stack, int *list_sort, int len)
{
	t_indice	cursor;
	int			i;

	i = 1;
	cursor.start = is_sort[START_SORT];
	cursor.after = cursor.start;
	ring_cursor(len, &cursor.after, TO_TOP, stack);
	while (cursor.after != is_sort[START_SORT])
	{
		if (stack->nb[cursor.start] > stack->nb[cursor.after])
		{
			list_sort[i] = stack->nb[cursor.after];
			i++;
			cursor.start = cursor.after;
		}
		ring_cursor(len, &cursor.after, TO_TOP, stack);
	}
}

/* Copy numbers wich stay in A in new array */
void	copy_list_sort(t_ring *stack, int *list_sort, int is_sort[4], int len)
{
	list_sort[0] = stack->nb[is_sort[START_SORT]];
	if (is_sort[DIRECTION] == MORE)
		more(is_sort, stack, list_sort, len);
	else
		less(is_sort, stack, list_sort, len);
}

/* Push numbers wich don't stay in A, in B */
void	clear_a(t_stack *stack, int len, int *list_sort, int is_sort[3])
{
	int			not_found;
	int			lenth;

	lenth = len;
	while (lenth > 0)
	{
		not_found = 0;
		while (not_found < is_sort[LENTH])
		{
			if (stack->a.nb[stack->a.top] != list_sort[not_found])
				not_found++;
			else
				break ;
		}
		if (not_found == is_sort[LENTH])
			p(&stack->a, &stack->b, len, "b");
		else
			r(&stack->a, len, "a");
		lenth--;
	}
}
