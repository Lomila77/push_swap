/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sa.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 15:14:48 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/08 15:14:48 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "instruction.h"
#include "libft/libft.h"
#include "utils.h"

int	s(t_ring *stack, int len, char *which_stack)
{
	int	previous_top;

	previous_top = stack->top;
	stack_cursor(len, &previous_top, TO_BOT, NULL);
	if (previous_top != stack->bot)
		swap_int(&stack->nb[stack->top], &stack->nb[previous_top]);
	if (which_stack != NULL)
	{
		ft_putstr("s");
		ft_putstr(which_stack);
		ft_putstr("\n");
	}
	return (SUCCESS);
}
