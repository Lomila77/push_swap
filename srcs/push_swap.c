/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/11 16:08:08 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/16 12:15:05 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft/libft.h"
#include "parsing.h"
#include "algo.h"
#include "utils.h"

int	push_swap(int len, char **arg)
{
	t_stack	stack;
	t_move	move;
	int		ret;

	ret = parsing(arg, len, &stack, NO_CHECK);
	if (ret == ERROR)
		return (ERROR);
	else if (ret == SORT)
		return (SORT);
	if (len == 3)
	{
		sort_3(&stack, len);
		return (SUCCESS);
	}
	stack.a.start = sort_a(&stack, len);
	while (stack.b.top != stack.b.bot)
	{
		move = is_to_insert(&stack, len);
		update_stack_for_insert(&stack, len, &move, "b");
		insert(&stack, len, &move);
	}
	update_top(&stack.a, get_smallest_val(&stack.a, len), len, "a");
	free(stack.a.nb);
	free(stack.b.nb);
	return (SUCCESS);
}
