/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   second_stage_algo.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/08 11:09:46 by gcolomer          #+#    #+#             */
/*   Updated: 2021/10/08 11:09:46 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "algo.h"
#include "utils.h"
#include "instruction.h"
#include "libft/libft.h"

/* Define wich move with wich numbers is the more profitable */
t_move	is_to_insert(t_stack *stack, int len)
{
	t_move	move;
	t_move	compare;

	compare.cursor = stack->b.top;
	move.a = 2147483647;
	move.b = 0;
	move.total = move.a + move.b;
	while (compare.cursor != stack->b.bot)
	{
		get_smallest_val(&stack->b, len);
		get_smallest_val(&stack->a, len);
		get_biggest_val(&stack->a, len);
		get_biggest_val(&stack->b, len);
		compare.b = position_in_stack(&stack->b, len, &compare);
		nb_move_for_insert(stack, len, &compare);
		compare.value = stack->b.nb[compare.cursor];
		if (compare.total < move.total)
			move_equals_to(&move, &compare);
		stack_cursor(len, &compare.cursor, TO_BOT, &stack->b);
	}
	return (move);
}

/* Calcul the best move for each numbers */
void	nb_move_for_insert(t_stack *stack, int len, t_move *move)
{
	int		nb_move[4];
	int		to_insert;

	to_insert = stack->b.nb[move->cursor];
	move->a_r = count_r_move(stack, len, to_insert);
	move->a_rr = count_rr_move(stack, len, to_insert);
	nb_move[R_R] = count_r_r_move(move);
	nb_move[RR_RR] = count_rr_rr_move(move);
	nb_move[RRA_RB] = move->a_rr + move->b_r;
	nb_move[RA_RRB] = move->a_r + move->b_rr;
	if (move->a_r == 0 || move->a_rr == 0)
		move->a = 0;
	if (move->a_r < move->a_rr)
	{
		move->instruction_a = R;
		move->a = move->a_r;
	}
	else
	{
		move->instruction_a = RR;
		move->a = move->a_rr;
	}
	move->total = move->a + move->b;
	find_smallest_move(nb_move, move);
}

/* Rotate stack to good place for insert */
void	update_stack_for_insert(t_stack *stack, int len, t_move *mv, char *name)
{
	while (stack->b.nb[stack->b.top] != mv->value)
	{
		if (mv->instruction_b == R && mv->instruction_a == R && mv->a > 0)
		{
			rr(stack, len, name);
			mv->a--;
		}
		else if (mv->instruction_b == R)
			r(&stack->b, len, name);
		else if (mv->instruction_a == RR && mv->instruction_b == RR
			&& mv->a > 0)
		{
			rrr(stack, len, name);
			mv->a--;
		}
		else
			rr_(&stack->b, len, name);
	}
}

/* Insert the good numbers with the good move */
int	insert(t_stack *stack, int len, t_move *move)
{
	int	i;

	i = 0;
	while (i != move->a)
	{
		if (move->instruction_a == R)
		{
			r(&stack->a, len, "a");
			i++;
		}
		else if (move->instruction_a == RR)
		{
			rr_(&stack->a, len, "a");
			i++;
		}
	}
	p(&stack->b, &stack->a, len, "a");
	return (SUCCESS);
}
