/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rr.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 18:41:42 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/09 18:41:42 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "instruction.h"
#include "libft/libft.h"

int	rr(t_stack *stack, int len, char *print)
{
	r(&stack->a, len, NULL);
	r(&stack->b, len, NULL);
	if (print != NULL)
		ft_putstr("rr\n");
	return (SUCCESS);
}
