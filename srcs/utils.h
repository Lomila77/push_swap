/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/10 12:24:40 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/10 12:24:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include "data.h"

/* In utils.c */
void	init_top_and_bot(t_ring *a, t_ring *b, int len);
void	stack_cursor(int len, int *opt, int mode, t_ring *stack);
void	ring_cursor(int len, int *param, int mode, t_ring *stack);
int		empty_stack(t_ring *stack);
int		get_biggest_val(t_ring *stack, int len);

/* In utils_bis.c */
void	update_top(t_ring *stack, int top, int len, char *name);
int		get_smallest_val(t_ring *stack, int len);
void	move_equals_to(t_move *it, t_move *is);

/* In utils_algo.c */
int		count_r_move(t_stack *stack, int len, int to_insert);
int		count_rr_move(t_stack *stack, int len, int to_insert);
void	count_list_sort(int *sort, t_ring *stack, int len);
int		position_in_stack(t_ring *stack, int len, t_move *compare);

/* In utils_algo_bis.c */
int		count_r_r_move(t_move *move);
int		count_rr_rr_move(t_move *move);
void	find_smallest_move(int nb_move[2], t_move *move);

/*  In utils_algo_ter.c */
void	array_is_equals(int *it, int *is, t_indice *cursor, int mode);
void	count_list_sort_rev(int	*sort, t_ring *stack, int len);
int		is_sort(t_ring *stack, int len);

#endif
