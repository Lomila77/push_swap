/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   p.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/14 13:19:12 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/14 13:19:12 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "instruction.h"
#include "libft/libft.h"
#include "utils.h"

int	p(t_ring *push, t_ring *stack, int len, char *which_stack)
{
	if (push->top == push->bot)
		return (SUCCESS);
	stack_cursor(len, &stack->top, TO_TOP, NULL);
	stack->nb[stack->top] = push->nb[push->top];
	stack_cursor(len, &push->top, TO_BOT, NULL);
	if (which_stack != NULL)
	{
		ft_putstr("p");
		ft_putstr(which_stack);
		ft_putstr("\n");
	}
	return (SUCCESS);
}
