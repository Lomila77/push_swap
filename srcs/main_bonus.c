/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/06 16:10:28 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/06 16:10:28 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"
#include "instruction.h"
#include "checker_bonus.h"
#include "libft/libft.h"
#include "utils.h"
#include "return.h"

#ifdef BONUS

int	set_matrice(t_stack *stack, int *len, char *arg, char **is_split)
{
	is_split = ft_split(arg, ' ');
	*len = len_matrice(is_split);
	if (empty_in_matrice(is_split) == YES || is_split == NULL)
	{
		free_matrice(is_split);
		return (ERROR);
	}
	if (parsing(is_split, *len, stack, CHECK) == ERROR)
	{
		free_matrice(is_split);
		return (ERROR);
	}
	free_matrice(is_split);
	return (SUCCESS);
}

int	main(int argc, char **argv)
{
	t_stack		stack;
	char		**is_split;
	int			len;

	is_split = NULL;
	len = argc - 1;
	if (argc >= 2 && (empty_in_matrice(argv) == YES))
		return (error(STDERR, NULL));
	else if (argc == 2 && str_have_space(argv[1]) != NO)
	{
		if (set_matrice(&stack, &len, argv[1], is_split) == ERROR)
			return (ERROR);
	}
	else if ((argc == 2 && str_is_num(argv[1]) == YES) || argc == 1)
		return (SUCCESS);
	else
		if (parsing(&argv[1], len, &stack, CHECK) == ERROR)
			return (ERROR);
	if (get_instruction(&stack, len) == ERROR)
		return (error(STDERR, &stack));
	check_sort(&stack, len);
	return (0);
}

#endif
