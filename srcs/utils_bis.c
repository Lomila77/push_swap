/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_bis.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/27 18:29:08 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/27 18:29:08 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "instruction.h"

int	move_top(t_ring *stack, int top, int len, int *move)
{
	int	cursor;
	int	v[2];

	cursor = stack->top;
	v[R] = 0;
	v[RR] = 0;
	while (stack->nb[cursor] != stack->nb[top])
	{
		stack_cursor(len, &cursor, TO_BOT, stack);
		v[R]++;
	}
	cursor = stack->bot;
	stack_cursor(len, &cursor, TO_TOP, stack);
	v[RR]++;
	while (stack->nb[cursor] != stack->nb[top])
	{
		stack_cursor(len, &cursor, TO_TOP, stack);
		v[RR]++;
	}
	*move = v[RR];
	if (v[RR] < v[R])
		return (RR);
	*move = v[R];
	return (R);
}

void	update_top(t_ring *stack, int top, int len, char *name)
{
	int	v;
	int	nb;

	nb = 0;
	if (top < 0)
		return ;
	v = move_top(stack, top, len, &nb);
	while (nb > 0)
	{
		if (v == RR)
			rr_(stack, len, name);
		else
			r(stack, len, name);
		nb--;
	}
}

int	get_smallest_val(t_ring *stack, int len)
{
	int	i;
	int	j;

	i = stack->top;
	j = i;
	stack->smallest = stack->nb[i];
	while (i != stack->bot)
	{
		if (stack->nb[i] < stack->smallest)
		{
			stack->smallest = stack->nb[i];
			j = i;
		}
		stack_cursor(len, &i, TO_BOT, stack);
	}
	return (j);
}

void	move_equals_to(t_move *it, t_move *is)
{
	it->cursor = is->cursor;
	it->instruction_a = is->instruction_a;
	it->instruction_b = is->instruction_b;
	it->a = is->a;
	it->a_r = is->a_r;
	it->a_rr = is->a_rr;
	it->b = is->b;
	it->b_r = is->b_r;
	it->b_rr = is->b_rr;
	it->total = is->total;
	it->value = is->value;
}
