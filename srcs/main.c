/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/05 19:46:41 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/05 19:46:41 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft/libft.h"
#include "return.h"

#ifndef BONUS

int	in_matrice(char **argv, char **is_split)
{
	int	ret;

	ret = 0;
	is_split = ft_split(argv[1], ' ');
	if (empty_in_matrice(is_split) == YES)
	{
		free_matrice(is_split);
		return (error(STDERR, NULL));
	}
	if (is_split == NULL)
		return (ERROR);
	ret = push_swap(len_matrice(is_split), is_split);
	if (ret <= ERROR)
	{
		free_matrice(is_split);
		if (ret == ERROR)
			exit(ERROR);
		else
			exit(SUCCESS);
	}
	return (SUCCESS);
}

int	main(int argc, char **argv)
{
	char	**is_split;

	is_split = NULL;
	if (argc < 2)
		return (SUCCESS);
	else if (empty_in_matrice(argv) == YES)
		return (error(STDERR, NULL));
	else if (argc == 2 && str_have_space(argv[1]) != NO)
	{
		if (in_matrice(argv, is_split) == ERROR)
			return (ERROR);
	}
	else if (argc == 2 && str_is_num(argv[1]) == YES)
		return (SUCCESS);
	else
		push_swap(argc - 1, &argv[1]);
	return (SUCCESS);
}

#endif
