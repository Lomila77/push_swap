/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_algo_bis.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 17:34:40 by gcolomer          #+#    #+#             */
/*   Updated: 2021/10/25 17:34:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "libft/libft.h"

int	count_r_r_move(t_move *move)
{
	if (move->a_r < move->b_r)
		return (move->b_r);
	else
		return (move->a_r);
}

int	count_rr_rr_move(t_move *move)
{
	if (move->a_rr < move->b_rr)
		return (move->b_rr);
	else
		return (move->a_rr);
}

void	init_move(int mva, int mvb, t_move *move)
{
	move->instruction_a = mva;
	move->instruction_b = mvb;
	if (mva == R)
		move->a = move->a_r;
	else if (mva == RR)
		move->a = move->a_rr;
	if (mvb == R)
		move->b = move->b_r;
	else if (mvb == RR)
		move->b = move->b_rr;
}

void	find_smallest_move(int nb_move[4], t_move *move)
{
	if (nb_move[R_R] < nb_move[RR_RR] && nb_move[R_R] < nb_move[RRA_RB]
		&& nb_move[R_R] < nb_move[RA_RRB])
	{
		init_move(R, R, move);
		move->total = nb_move[R_R];
	}
	else if (nb_move[RR_RR] < nb_move[R_R]
		&& nb_move[RR_RR] < nb_move[RRA_RB] && nb_move[RR_RR] < nb_move[RA_RRB])
	{
		init_move(RR, RR, move);
		move->total = nb_move[RR_RR];
	}
	else if (nb_move[RRA_RB] < nb_move[R_R] && nb_move[RRA_RB] < nb_move[RR_RR]
		&& nb_move[RRA_RB] < nb_move[RA_RRB])
	{
		init_move(RR, R, move);
		move->total = nb_move[RRA_RB];
	}
	else if (nb_move[RA_RRB] < nb_move[R_R]
		&& nb_move[RA_RRB] < nb_move[RR_RR]
		&& nb_move[RA_RRB] < nb_move[RRA_RB])
	{
		init_move(R, RR, move);
		move->total = nb_move[RA_RRB];
	}
}
