/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/05 18:31:14 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/05 18:31:14 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"
#include "libft/libft.h"
#include "utils.h"
#include "return.h"

int	check_stack(t_stack *stack, int argc, int mode)
{
	int	indice[2];

	if (dup_in_array(stack->a.nb, argc + 1, indice) == YES
		&& (indice[0] != stack->a.bot && indice[1] != stack->a.bot))
		return (error(STDERR, stack));
	if (is_sort(&stack->a, argc) == YES && mode != CHECK)
	{
		free(stack->a.nb);
		free(stack->b.nb);
		return (SORT);
	}
	return (SUCCESS);
}

int	parsing(char **argv, int argc, t_stack *stack, int mode)
{
	int	i;
	int	j;

	i = 0;
	while (argv[i] && str_is_num(argv[i]) == YES)
		i++;
	if (i != argc)
		return (error(STDERR, NULL));
	i--;
	stack->a.nb = ft_calloc(argc + 1, sizeof(int));
	stack->b.nb = ft_calloc(argc + 1, sizeof(int));
	if (stack->a.nb == NULL || stack->b.nb == NULL)
		return (ERROR);
	init_top_and_bot(&stack->a, &stack->b, argc);
	j = stack->a.bot;
	while (CONTINUE)
	{
		stack_cursor(argc, &j, TO_TOP, &stack->a);
		if (ft_atoi(argv[i], &stack->a.nb[j]) == ERROR)
			return (error(STDERR, stack));
		if (j == stack->a.top)
			break ;
		i--;
	}
	return (check_stack(stack, argc, mode));
}
