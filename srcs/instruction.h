/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instruction.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 15:40:45 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/08 15:40:45 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTION_H
# define INSTRUCTION_H

# include "data.h"

int		call_instruction(t_stack *stack, char *instruction, int len);
int		p(t_ring *push, t_ring *stack, int len, char *which_stack);
int		s(t_ring *stack, int len, char *which_stack);
int		ss(t_stack *stack, int len, char *print);
int		r(t_ring *stack, int len, char *which_stack);
int		rr(t_stack *stack, int len, char *print);
int		rr_(t_ring *stack, int len, char *which_stack);
int		rrr(t_stack *stack, int len, char *print);

#endif
