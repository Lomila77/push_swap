/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_algo_ter.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/15 16:08:49 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/15 16:08:49 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "libft/libft.h"

void	init_var_rev(int tmp[3], t_indice *cursor, t_ring *stack, int len)
{
	tmp[LENTH] = 1;
	cursor->after = cursor->start;
	ring_cursor(len, &cursor->after, TO_TOP, stack);
	tmp[NB] = stack->nb[cursor->start];
	tmp[VALUE_START] = tmp[NB];
}

void	array_is_equals(int *it, int *is, t_indice *cursor, int mode)
{
	it[LENTH] = is[LENTH];
	it[VALUE_START] = is[VALUE_START];
	it[START_SORT] = cursor->start;
	it[DIRECTION] = mode;
}

void	count_list_sort_rev(int	*sort, t_ring *stack, int len)
{
	t_indice	cursor;
	int			tmp[4];

	cursor.start = stack->bot;
	stack_cursor(len, &cursor.start, TO_TOP, NULL);
	while (cursor.start != stack->bot)
	{
		init_var_rev(tmp, &cursor, stack, len);
		while (cursor.after != cursor.start)
		{
			if (tmp[NB] > stack->nb[cursor.after])
			{
				tmp[LENTH]++;
				tmp[NB] = stack->nb[cursor.after];
			}
			ring_cursor(len, &cursor.after, TO_TOP, stack);
		}
		if (tmp[LENTH] > sort[LENTH])
		{
			array_is_equals(sort, tmp, &cursor, LESS);
		}
		stack_cursor(len, &cursor.start, TO_TOP, stack);
	}
}

int	is_sort(t_ring *stack, int len)
{
	int	cursor;
	int	tmp;

	cursor = stack->top;
	tmp = stack->nb[cursor];
	stack_cursor(len, &cursor, TO_BOT, NULL);
	while (cursor != stack->bot)
	{
		if (tmp > stack->nb[cursor])
			return (NO);
		tmp = stack->nb[cursor];
		stack_cursor(len, &cursor, TO_BOT, NULL);
	}
	return (YES);
}
