/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   r.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 18:50:06 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/09 18:50:06 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"
#include "instruction.h"
#include "utils.h"

int	r(t_ring *stack, int len, char *which_stack)
{
	if (stack->top == stack->bot)
		return (SUCCESS);
	swap_int(&stack->nb[stack->top], &stack->nb[stack->bot]);
	stack_cursor(len, &stack->bot, TO_BOT, NULL);
	stack_cursor(len, &stack->top, TO_BOT, NULL);
	if (which_stack != NULL)
	{
		ft_putstr("r");
		ft_putstr(which_stack);
		ft_putstr("\n");
	}
	return (SUCCESS);
}
