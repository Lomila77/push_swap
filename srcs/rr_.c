/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rr_.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/21 11:52:00 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/21 11:52:00 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "instruction.h"
#include "libft/libft.h"
#include "utils.h"

int	rr_(t_ring *stack, int len, char *which_stack)
{
	int	bot;
	int	top;

	if (stack->top == stack->bot)
		return (SUCCESS);
	bot = stack->bot;
	top = stack->top;
	stack_cursor(len, &bot, TO_TOP, NULL);
	stack_cursor(len, &top, TO_TOP, NULL);
	if (stack->top == bot)
		return (SUCCESS);
	stack->nb[top] = stack->nb[bot];
	stack_cursor(len, &stack->top, TO_TOP, NULL);
	stack_cursor(len, &stack->bot, TO_TOP, NULL);
	if (which_stack != NULL)
	{
		ft_putstr("rr");
		ft_putstr(which_stack);
		ft_putstr("\n");
	}
	return (SUCCESS);
}
