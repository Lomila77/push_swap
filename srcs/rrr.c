/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rrr.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/11 17:31:51 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/11 17:31:51 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "instruction.h"
#include "libft/libft.h"

int	rrr(t_stack *stack, int len, char *print)
{
	rr_(&stack->a, len, NULL);
	rr_(&stack->b, len, NULL);
	if (print != NULL)
		ft_putstr("rrr\n");
	return (SUCCESS);
}
