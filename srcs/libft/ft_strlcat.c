/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/14 17:57:09 by gcolomer          #+#    #+#             */
/*   Updated: 2020/12/14 17:57:12 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, char *src, size_t size)
{
	size_t	ldst;
	size_t	a;
	size_t	lsrc;

	ldst = 0;
	a = 0;
	lsrc = 0;
	while (ldst < size && dest[ldst])
		ldst++;
	while (src[lsrc])
		lsrc++;
	if (size == 0 || ldst >= size)
		return (ldst + lsrc);
	while (src[a] && a + ldst < size - 1)
	{
		dest[ldst + a] = src[a];
		a++;
	}
	dest[ldst + a] = '\0';
	return (lsrc + ldst);
}
