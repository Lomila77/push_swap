/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nnstrchr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/02 13:34:08 by gcolomer          #+#    #+#             */
/*   Updated: 2021/04/02 13:34:08 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_nnstrchr(const char *s, int c)
{
	char	ch;
	char	*str;
	int		i;

	ch = c;
	str = (char *)s;
	i = 0;
	while (str[i])
	{
		if (ch == str[i])
			return (i + 1);
		i++;
	}
	if (str[i] == ch)
		return (i + 1);
	return (0);
}
