/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/14 17:33:15 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:29:13 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <stdlib.h>

# define SUCCESS 0
# define ERROR -1

# define EOF 0

# define ERROR_N NULL

# define STDIN 0
# define STDOUT 1
# define STDERR 2

# define NO -1
# define YES 1

# define CONTINUE 1

# define BUFFER_SIZE 6

typedef struct s_list
{
	void			*content;
	struct s_list	*next;
}	t_list;

typedef struct s_gnl
{
	char	buffer[BUFFER_SIZE + 1];
	char	*tmp[2];
}	t_gnl;

void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);
void				*ft_memmove(void *dest, const void *src, size_t len);
size_t				ft_strlen(const char *str);
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_isspace(char c);
int					ft_strncmp(char *s1, char *s2, size_t n);
size_t				ft_strlcat(char *dest, char *src, size_t size);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_atoi(const char *str, int *nb);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
char				*ft_strchr(const char *s, int c);
size_t				ft_strlcpy(char *dest, const char *src, size_t dstsize);
char				*ft_strnstr(const char *haystack, const char *needle,
						size_t len);
char				*ft_strrchr(const char *s, int c);
int					ft_tolower(int c);
int					ft_toupper(int c);
char				*ft_substr(char *str, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
void				*ft_calloc(size_t count, size_t size);
char				*ft_itoa(int n);
void				ft_putchar_fd(char c, int ft);
void				ft_putstr(char *str);
char				**ft_split(char *src, char c);
char				*ft_strdup(char *src);
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char				*ft_strtrim(char const *s1, char const *set);
void				ft_putstr_fd(char *s, int fd);
void				ft_putendl_fd(char *s, int fd);
void				ft_putnbr_fd(int n, int fd);
t_list				*ft_lstnew(void *content);
void				ft_lstadd_front(t_list **alst, t_list *new);
int					ft_lstsize(t_list *lst);
t_list				*ft_lstlast(t_list *lst);
void				ft_lstadd_back(t_list **alst, t_list *new);
void				ft_lstdelone(t_list *lst, void (*del)(void *));
void				ft_lstclear(t_list **lst, void (*del)(void *));
void				ft_lstiter(t_list *lst, void (*f)(void *));
t_list				*ft_lstmap(t_list *lst, void *(*f)(void *),
						void (*del)(void *));
size_t				ft_nstrchr(const char *s, int c);
size_t				ft_nnstrchr(const char *s, int c);
int					ft_isnum(int c);
int					ft_batoi(const char *str, int length);
unsigned char		*ft_sitoa(long long int n);
unsigned char		*ft_uitoa(unsigned long long int n);
int					ft_str_isnegnum(unsigned char *str);
unsigned long long	ft_atoiu(const char *str);
double				ft_atod(const char *str);
int					ft_space(char c);
int					ft_isn(char c);
char				*ft_strccpy(char *str, char c);
int					ft_count(char c, char *str);
char				*ft_strjoin_sep(char *s1, char *s2, char c);
int					str_have_space(char *str);
int					str_have_c(char *str, char c);
int					count_char(char *str, char c);
char				*ft_strlnew(char *str, char del);
int					ft_free(char **to_free);
int					str_is_num(char *str);
int					dup_in_array(int *array, int len, int indice[2]);
int					n_read(int fd, char **buf, int len, int *ret);
void				swap_int(int *a, int *b);
int					mult_free_err(char *f1, char *f2, char *f3, char *f4);
void				be_null4(char **s1, char **s2, char **s3, char **s4);
void				be_null2(char **s1, char **s2);
void				init_array_to_0(int	*array, int len);
int					len_matrice(char **matrice);
int					empty_in_matrice(char **matrice);
void				free_matrice(char **matrice);
#endif
