/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_isnegnum.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 10:15:14 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:15:16 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_str_isnegnum(unsigned char *str)
{
	while (*str != '-' && *str)
		str++;
	if (*str == '-')
		return (YES);
	return (NO);
}
