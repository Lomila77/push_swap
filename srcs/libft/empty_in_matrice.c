/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   empty_in_matrice.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/19 18:41:06 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/19 18:41:06 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	empty_in_matrice(char **matrice)
{
	int	i;

	i = 0;
	while (matrice[i] != NULL)
	{
		if (matrice[i][0] == '\0')
			return (YES);
		i++;
	}
	return (NO);
}
