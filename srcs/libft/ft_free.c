/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/29 15:37:31 by gcolomer          #+#    #+#             */
/*   Updated: 2021/08/29 15:37:31 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_free(char **to_free)
{
	if (to_free)
	{
		free(*to_free);
		*to_free = NULL;
		return (SUCCESS);
	}
	return (ERROR);
}
