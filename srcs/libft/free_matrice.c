/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_matrice.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/22 11:31:25 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/22 11:31:25 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	free_matrice(char **matrice)
{
	int	i;

	i = 0;
	if (matrice != NULL)
	{
		while (matrice[i] != NULL)
		{
			ft_free(&matrice[i]);
			i++;
		}
		free(matrice);
	}
}
