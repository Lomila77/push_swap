/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atod.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/30 15:19:36 by gcolomer          #+#    #+#             */
/*   Updated: 2021/03/30 15:19:36 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "math.h"

#include <stdio.h>

int	trad(const char *str, double *res, int i)
{
	int	p;
	int	exp;

	p = 0;
	exp = 0;
	while ((ft_isdigit(str[i]) || (str[i] == '.' && p < 1)) && str[i])
	{
		if (str[i] != '.')
			*res = (*res) * 10 + (str[i] - 48);
		if (str[i] == '.')
			p = 1;
		else if (p == 1)
			exp++;
		i++;
	}
	return (exp);
}

double	ft_atod(const char *str)
{
	int		i;
	int		val;
	int		exp;
	double	res;

	i = 0;
	val = 0;
	res = 0;
	exp = 0;
	while (str[i] == '\t' || str[i] == '\n' || str[i] == '\v'
		|| str[i] == '\f' || str[i] == '\r' || str[i] == ' ')
		i++;
	if (str[i] == '-')
	{
		val = 1;
		i++;
	}
	else if (str[i] == '+')
		i++;
	exp = trad(str, &res, i);
	res *= pow(10, -exp);
	if (val > 0)
		res *= -1;
	return (res);
}
