/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   n_read.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/06 16:18:45 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/06 16:18:45 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

int	n_read(int fd, char **buf, int len, int *ret)
{
	*ret = read(fd, *buf, len);
	if (*ret == -1)
		return (ERROR);
	else if (*ret == 0)
		return (EOF);
	return (1);
}
