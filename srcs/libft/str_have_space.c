/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_have_space.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/28 16:45:59 by gcolomer          #+#    #+#             */
/*   Updated: 2021/07/28 16:45:59 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	str_have_space(char *str)
{
	int	i;
	int	space;

	i = 0;
	space = 0;
	while (str[i])
	{
		if (ft_isspace(str[i]) == YES)
			space++;
		i++;
	}
	if (space > 0)
		return (space);
	return (NO);
}
