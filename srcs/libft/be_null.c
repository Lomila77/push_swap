/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   be_null4.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 11:02:23 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/10 11:02:23 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	be_null4(char **s1, char **s2, char **s3, char **s4)
{
	*s1 = NULL;
	*s2 = NULL;
	*s3 = NULL;
	*s4 = NULL;
}

void	be_null2(char **s1, char **s2)
{
	*s1 = NULL;
	*s2 = NULL;
}
