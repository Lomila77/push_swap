/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_sep.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/27 21:45:05 by gcolomer          #+#    #+#             */
/*   Updated: 2021/07/27 21:45:05 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin_sep(char *s1, char *s2, char c)
{
	int		len;
	char	*str;

	len = ft_strlen(s1) + ft_strlen(s2) + 2;
	str = ft_calloc(len, sizeof(char));
	if (str == NULL)
		return (str);
	ft_strlcat(str, s1, len);
	str[ft_strlen(s1)] = c;
	ft_strlcat(str, s2, len);
	return (str);
}
