/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlnew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/19 18:59:35 by gcolomer          #+#    #+#             */
/*   Updated: 2021/08/19 18:59:35 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/* !!! Cette fonction free str !!! */
char	*ft_strlnew(char *str, char del)
{
	char	*line;
	int		len;
	int		i;

	i = 0;
	len = 0;
	if (!str)
		return (ERROR_N);
	while (str[len] != del && str[len])
		len++;
	line = ft_calloc(len + 1, sizeof(char));
	if (line == NULL)
		return (ERROR_N);
	while (str[i] && str[i] != del)
	{
		line[i] = str[i];
		i++;
	}
	if (str)
		ft_free(&str);
	return (line);
}
