/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uitoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 10:05:20 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:14:21 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static unsigned long long	ft_wneg(unsigned long long n, unsigned char *str)
{
	unsigned long long	nbr;

	if (n < 0)
	{
		nbr = n * -1;
		str[0] = '-';
	}
	else
		nbr = n;
	return (nbr);
}

static unsigned long long	ft_neg(unsigned long long n,
		unsigned long long *val)
{
	unsigned long long	nbr;

	if (n < 0)
	{
		*val = 1;
		nbr = n * -1;
	}
	else
		nbr = n;
	return (nbr);
}

unsigned char	*ft_uitoa(unsigned long long n)
{
	unsigned long long	nbr;
	unsigned long long	len;
	unsigned char		*str;

	len = 0;
	nbr = ft_neg(n, &len);
	while (nbr >= 10)
	{
		nbr /= 10;
		len++;
	}
	len++;
	str = malloc(sizeof(char) * (len + 1));
	if (str == ERROR_N)
		return (ERROR_N);
	nbr = ft_wneg(n, str);
	str[len] = '\0';
	while (nbr >= 10)
	{
		str[--len] = nbr % 10 + 48;
		nbr /= 10;
	}
	str[--len] = nbr + 48;
	return (str);
}
