/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mult_free_err.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 14:08:45 by gcolomer          #+#    #+#             */
/*   Updated: 2021/10/18 14:08:45 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	mult_free_err(char *f1, char *f2, char *f3, char *f4)
{
	if (f1 != NULL)
		ft_free(&f1);
	if (f2 != NULL)
		ft_free(&f2);
	if (f3 != NULL)
		ft_free(&f3);
	if (f4 != NULL)
		ft_free(&f4);
	return (ERROR);
}
