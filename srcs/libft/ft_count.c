/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/27 21:27:25 by gcolomer          #+#    #+#             */
/*   Updated: 2021/07/27 21:27:25 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_count(char c, char *str)
{
	int	i;

	i = 0;
	if (!str)
		return (ERROR);
	while (str[i] != c && i != (int)ft_strlen(str))
		i++;
	if (!str[i] && str[i] != c)
		return (ERROR);
	if (i == 0)
		i = 1;
	return (i);
}
