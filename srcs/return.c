/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   return.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/15 11:09:30 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/15 11:09:30 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "return.h"
#include "libft/libft.h"

int	error(int fd, t_stack *stack)
{
	if (fd == STDERR)
	{
		ft_putstr_fd("Error\n", fd);
		all_free(stack);
	}
	else
		ft_putstr("KO\n");
	return (ERROR);
}

int	success(t_stack *stack)
{
	ft_putstr("OK\n");
	all_free(stack);
	return (SUCCESS);
}

int	all_free(t_stack *stack)
{
	if (stack != NULL)
	{
		if (stack->a.nb != NULL && stack->b.nb != NULL)
		{
			free(stack->a.nb);
			free(stack->b.nb);
			stack->a.nb = NULL;
			stack->b.nb = NULL;
		}
	}
	return (SUCCESS);
}
