/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ss.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/09 18:38:21 by gcolomer          #+#    #+#             */
/*   Updated: 2021/09/09 18:38:21 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "instruction.h"
#include "libft/libft.h"

/* swap first elements of pile A and pile B */
int	ss(t_stack *stack, int len, char *print)
{
	s(&stack->a, len, NULL);
	s(&stack->b, len, NULL);
	if (print != NULL)
		ft_putstr("ss\n");
	return (SUCCESS);
}
