/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_algo.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/15 11:03:49 by gcolomer          #+#    #+#             */
/*   Updated: 2021/11/16 12:31:13 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "libft/libft.h"

int	count_rr_move(t_stack *stack, int len, int to_insert)
{
	int	border[2];
	int	nb_move;

	nb_move = 0;
	border[TOP] = stack->a.bot;
	stack_cursor(len, &border[TOP], TO_TOP, &stack->a);
	border[BOT] = stack->a.top;
	while (!(to_insert > stack->a.nb[border[TOP]]
			&& to_insert < stack->a.nb[border[BOT]]))
	{
		if ((to_insert < stack->a.smallest || to_insert > stack->a.biggest)
			&& stack->a.smallest == stack->a.nb[border[BOT]])
			return (nb_move);
		nb_move++;
		stack_cursor(len, &border[TOP], TO_TOP, &stack->a);
		ring_cursor(len, &border[BOT], TO_TOP, &stack->a);
	}
	return (nb_move);
}

int	count_r_move(t_stack *stack, int len, int to_insert)
{
	int	border[2];
	int	nb_move;

	nb_move = 0;
	border[TOP] = stack->a.bot;
	stack_cursor(len, &border[TOP], TO_TOP, &stack->a);
	border[BOT] = stack->a.top;
	while (!(to_insert > stack->a.nb[border[TOP]]
			&& to_insert < stack->a.nb[border[BOT]]))
	{
		if ((to_insert < stack->a.smallest || to_insert > stack->a.biggest)
			&& stack->a.smallest == stack->a.nb[border[BOT]])
			return (nb_move);
		nb_move++;
		ring_cursor(len, &border[TOP], TO_BOT, &stack->a);
		stack_cursor(len, &border[BOT], TO_BOT, &stack->a);
	}
	return (nb_move);
}

void	init_var(int tmp[3], t_indice *cursor, t_ring *stack, int len)
{
	tmp[LENTH] = 1;
	cursor->after = cursor->start;
	ring_cursor(len, &cursor->after, TO_BOT, stack);
	tmp[NB] = stack->nb[cursor->start];
	tmp[VALUE_START] = tmp[NB];
}

void	count_list_sort(int	*sort, t_ring *stack, int len)
{
	t_indice	cursor;
	int			tmp[4];

	sort[LENTH] = 1;
	cursor.start = stack->top;
	while (cursor.start != stack->bot)
	{
		init_var(tmp, &cursor, stack, len);
		while (cursor.after != cursor.start)
		{
			if (tmp[NB] < stack->nb[cursor.after])
			{
				tmp[LENTH]++;
				tmp[NB] = stack->nb[cursor.after];
			}
			ring_cursor(len, &cursor.after, TO_BOT, stack);
		}
		if (tmp[LENTH] > sort[LENTH])
		{
			array_is_equals(sort, tmp, &cursor, MORE);
		}
		stack_cursor(len, &cursor.start, TO_BOT, stack);
	}
}

int	position_in_stack(t_ring *stack, int len, t_move *compare)
{
	int	cursor;
	int	nb[2];
	int	to_insert;

	to_insert = stack->nb[compare->cursor];
	cursor = stack->top;
	init_array_to_0(nb, 2);
	while (stack->nb[cursor] != to_insert)
	{
		ring_cursor(len, &cursor, TO_BOT, stack);
		nb[R]++;
	}
	cursor = stack->top;
	while (stack->nb[cursor] != to_insert)
	{
		ring_cursor(len, &cursor, TO_TOP, stack);
		nb[RR]++;
	}
	compare->b_r = nb[R];
	compare->b_rr = nb[RR];
	compare->instruction_b = R;
	if (nb[R] < nb[RR])
		return (nb[R]);
	compare->instruction_b = RR;
	return (nb[RR]);
}
