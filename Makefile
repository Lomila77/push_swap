NAME		= push_swap
NAME_B		= checker

SRCS		=	srcs/main.c \
			srcs/parsing.c \
			srcs/push_swap.c \
			srcs/first_stage_algo.c \
			srcs/second_stage_algo.c \
			srcs/instruction.c \
			srcs/r.c \
			srcs/rr_.c \
			srcs/rr.c \
			srcs/p.c \
			srcs/rrr.c \
			srcs/s.c \
			srcs/ss.c \
			srcs/utils.c \
			srcs/utils_bis.c \
			srcs/utils_algo.c \
			srcs/utils_algo_bis.c \
			srcs/utils_algo_ter.c \
			srcs/small_algo.c \
			srcs/return.c

SRCB		=	srcs/main_bonus.c \
			srcs/checker_bonus.c \
			srcs/parsing.c \
			srcs/push_swap.c \
			srcs/first_stage_algo.c \
			srcs/second_stage_algo.c \
			srcs/instruction.c \
			srcs/r.c \
			srcs/rr_.c \
			srcs/rr.c \
			srcs/p.c \
			srcs/rrr.c \
			srcs/s.c \
			srcs/ss.c \
			srcs/utils.c \
			srcs/utils_bis.c \
			srcs/utils_algo.c \
			srcs/utils_algo_bis.c \
			srcs/utils_algo_ter.c \
			srcs/small_algo.c \
			srcs/return.c


OBJS		= $(SRCS:.c=.o)
OBJB		= $(SRCB:.c=.o)
LIBFT		= srcs/libft/libft.a
CC			= clang
CFLAGS		= -Wall -Wextra -Werror #-g -fsanitize=address
INCLUDE		= -I srcs

all :		${NAME}
.PHONY :	all

%.o :		%.c
			${CC} ${CFLAGS} -c $< -o $@ ${INCLUDE}

${NAME} :	${OBJS}
			echo "Compilation libft en cours 🚧"
			make -C srcs/libft -f Makefile
			echo "Compilation push_swap en cours 🚧"
			${CC} ${CFLAGS} ${OBJS} ${LIBFT} ${INCLUDE} -o ${NAME}
			echo "Compilation terminée 🥳"

${NAME_B} :	${OBJB}
			make -C srcs/libft -f Makefile
			echo "Compilation checker en cours 🚧"
			${CC} ${CFLAGS} ${OBJB} ${LIBFT} ${INCLUDE} -o ${NAME_B}
			echo "Compilation terminée 🥳"

clean :
			rm -f ${OBJS} ${OBJB}
			make fclean -C srcs/libft -f Makefile
			echo "Nettoyage des '.o' 🗑"
.PHONY:		clean

fclean :	clean
			rm -f ${NAME}
			rm -f ${NAME_B}
			echo "Destruction de l'exec 🔫"
.PHONY :	fclean

re :		fclean all
.PHONY :	re

bonus :		CFLAGS += -D BONUS
bonus :		${NAME_B}
.PHONY:		bonus

.SILENT:
